FROM arm32v7/ubuntu:18.04 AS build

ENV MONERO_VERSION=0.17.1.1 MONERO_SHA256=3febba3fce8d51f63e3fc7736cfcce4987f740499baf3e6377182012b07d71d4
ENV ARCH=armv7

RUN apt-get update && apt-get install -y curl bzip2

WORKDIR /root
RUN curl https://dlsrc.getmonero.org/cli/monero-linux-$ARCH-v$MONERO_VERSION.tar.bz2 -O &&\
echo "$MONERO_SHA256  monero-linux-$ARCH-v$MONERO_VERSION.tar.bz2" | sha256sum -c - &&\
tar -xvf monero-linux-$ARCH-v$MONERO_VERSION.tar.bz2 --strip 1 --wildcards --no-anchored 'monerod' &&\
rm monero-linux-$ARCH-v$MONERO_VERSION.tar.bz2

FROM arm32v7/ubuntu:18.04

RUN useradd -ms /bin/bash monero && mkdir -p /home/monero/.bitmonero && chown -R monero:monero /home/monero/.bitmonero
USER monero
WORKDIR /home/monero

COPY --chown=monero:monero --from=build /root/monerod /home/monero/monerod

# blockchain loaction
VOLUME /home/monero/.bitmonero

EXPOSE 18080 18081

ENTRYPOINT ["./monerod"]
CMD ["--non-interactive", "--restricted-rpc", "--rpc-bind-ip=0.0.0.0", "--confirm-external-bind"]

